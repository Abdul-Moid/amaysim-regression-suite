Amaysim Regression Suite
====================

Setup
-----
Import the source code as Maven project, run "mvn clean install" to install maven dependency.
Online repository:  git clone https://Abdul-Moid@bitbucket.org/Abdul-Moid/amaysim-regression-suite.git
UserName: moid125@hotmail.com
Pwd: @maysim786


Automation Test Structure
------------------------

* Feature files:  feature files are written using Gherkin language based on the behavior-driven development (BDD) style of Given, When, Then. Cucumber-JVM consumes feature files to generate test code.

* Step definitions: implement feature files in java code

* Page objects: page objects are used to abstract the interactions between a user and a web page. Selenium is used to drive the browser.

* Yaml files: yaml files are used to manage test parameters, you can define the test environment URL, test data and message etc.


Execute test scenarios
----------------------
To execute tests, execute one of the following commands: 


*  mvn test -DEnv="production"
-- test against production env

* mvn test -DBrowser="firefox" 
-- test against firefox, options can be: IE, firefox, chrome.

* mvn test
-- by default test against production env, firefox browser

* mvn test -Dcucumber.options="--tags @login" 
-- execute specified test scenarios by defined by tags

* mvn test -Dcucumber.options="@target/rerun.txt"  
-- re-run failed test scenarios

Cucumber report
---------------
After executing tests, navigate to below folder to check pretty cucumber html reports, screenshot is captured if scenario marked as failed: 

/Target/amaysim-cucumber-report
* feature-overview.html
* amaysim-tests-XXXX.feature.html
* tag-overview.html

Important Information
---------------------
* add your feature files under src/test/resources/features/
* add your step definition under src/test/java/stepdefinitions/
* add your page object under src/test/java/com/amaysim/pages/
* After releasing features, tag with @cvt