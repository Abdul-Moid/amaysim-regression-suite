package com.amaysim.stepdefinitions;

import java.util.Map;

import org.junit.Assert;
import com.amaysim.config.ResourceManager;
import com.amaysim.pages.HomePage;
import com.amaysim.pages.LoginPage;
import com.amaysim.pages.MyAccountPage;
import com.amaysim.pages.MySettingsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyAccountSteps extends BaseSteps {

	private HomePage homePage = new HomePage(driver);
	private LoginPage loginPage = new LoginPage(driver);
	private MyAccountPage myAccountPage = new MyAccountPage(driver);
	private MySettingsPage mySettingsPage = new MySettingsPage(driver);

	private String homePage_Url = resourceManager.getUrl().get("amaysimhomepage_url");
	private Map<String, String> login = resourceManager.getTestData().get("login");

	String msn = String.valueOf(login.get("msn"));
	String pwd = String.valueOf(login.get("pwd"));
	String divert = String.valueOf(login.get("divert"));

	public MyAccountSteps(ResourceManager resourceManager) {
		super(resourceManager);
	}

	@Given("^I open amaysim home page$")
	public void i_open_amaysim_home_page() throws Throwable {
		driver.get(homePage_Url);
	}

	@When("^I navigate to login page$")
	public void i_navigate_to_login_page() throws Throwable {
		homePage.clickMyAmaysimNavElement();
	}

	@When("^I enter my valid login details$")
	public void i_enter_my_valid_login_details() throws Throwable {
		loginPage.setAmaysimNumber(msn);
		loginPage.setAmaysimPassword(pwd);
		loginPage.clickLoginButton();
	}

	@Then("^I should see my account page$")
	public void i_should_see_my_account_page() throws Throwable {
		myAccountPage.verifyLoginMsn(msn);
	}

	@Then("^I navigate to my settings page$")
	public void i_navigate_to_my_settings_page() throws Throwable {
		myAccountPage.clickMySettingsLink();
	}


	@Then("^I update call forwading settings$")
	public void i_update_call_forwading_settings() throws Throwable {
		mySettingsPage.callForwardingEnableDisable(divert);
	}

	@Then("^I logout$")
	public void i_logout() throws Throwable {
		myAccountPage.clickLogoutLink();
	}
}
