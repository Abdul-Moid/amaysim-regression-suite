package com.amaysim.stepdefinitions;

import org.openqa.selenium.WebDriver;
import com.amaysim.config.ResourceManager;
import com.amaysim.support.Hooks;

public class BaseSteps {

	protected WebDriver driver;
	protected ResourceManager resourceManager;

	public BaseSteps(ResourceManager resourceManager) {
		this.driver = Hooks.driver;
		this.resourceManager = resourceManager;
	}

}
