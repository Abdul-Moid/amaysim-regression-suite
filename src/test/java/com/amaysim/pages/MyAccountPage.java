package com.amaysim.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage extends BasePage {

	public MyAccountPage(WebDriver driver) {
		super(driver);

	}

	public String getLoginAccountNumber() {
		new WebDriverWait(driver, 25).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".account-top>a>div")));
		return this.driver.findElements(By.cssSelector(".account-top>a>div")).get(1).getText();
	}

	public void clickMySettingsLink() {
		this.driver.findElement(By.linkText("My Settings")).click();
	}

	public void clickLogoutLink() {
		this.driver.findElement(By.linkText("Logout")).click();
	}
	
	public void verifyLoginMsn(String msn) {
		Assert.assertTrue("Number Matched", this.getLoginAccountNumber().contains(msn));
	}
}
