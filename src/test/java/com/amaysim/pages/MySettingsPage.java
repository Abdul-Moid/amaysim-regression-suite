package com.amaysim.pages;

import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MySettingsPage extends BasePage {


	public MySettingsPage(WebDriver driver) {
		super(driver);

	}

	public void clickCallForwadingEditLink() {

		this.driver.findElement(By.id("edit_settings_call_forwarding")).click();

		Set<String> windowId = driver.getWindowHandles();
		Iterator<String> itererator = windowId.iterator();
		String newWinID = itererator.next();

		// newWinID = itererator.next();

		driver.switchTo().window(newWinID);

		this.driver.findElement(By.linkText("Confirm")).click();
	}

	public WebElement getForwarwdCallsElement() {
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.id("my_amaysim2_setting_call_divert_number")));
		return this.driver.findElement(By.id("my_amaysim2_setting_call_divert_number"));

	}

	public void setForwardCallsElement(String divertNumber) {
		this.getForwarwdCallsElement().sendKeys(divertNumber);
		this.driver.findElement(By.name("commit")).click();
	}
	
	public void verifySuccessPopup() {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".form_info_popup.open")));
		Assert.assertEquals("Setting updated", "Success", this.driver.findElement(By.cssSelector(".form_info_popup.open>h1")).getText());
	}

	public void callForwardingEnableDisable(String divertNumber) throws InterruptedException {
		String callForwardingStatus = this.driver
				.findElement(By.cssSelector("#settings_call_forwarding .setting-option-value-text")).getText();

		if (callForwardingStatus.equals("No")) {
			this.clickCallForwadingEditLink();
			this.setForwardCallsElement(divertNumber);
			this.verifySuccessPopup();
			String divertedNum = this.driver
					.findElement(By.cssSelector("#settings_call_forwarding .setting-option-details-text")).getText();
			Assert.assertTrue("Number Matched", divertedNum.contains(divertNumber));
		}

		else {
			this.clickCallForwadingEditLink();


			WebElement elem = driver.findElement(By.xpath("//input[@id='my_amaysim2_setting_call_divert_false']"));
			// String js = "arguments[0].style.height='auto';
			// arguments[0].style.visibility='visible';";
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].click();", elem);
			this.driver.findElement(By.name("commit")).click();
			this.verifySuccessPopup();
		}
	}
}
