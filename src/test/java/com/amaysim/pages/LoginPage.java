package com.amaysim.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {

	public LoginPage(WebDriver driver) {
		super(driver);

	}

	public WebElement getAmaysimNumberElement() {
		return this.driver.findElement(By.id("my_amaysim2_user_session_login"));
	}

	public void setAmaysimNumber(String msn) {
		this.getAmaysimNumberElement().sendKeys(msn);
	}

	public WebElement getLoginElement() {
		return this.driver.findElement(By.name("commit"));
	}

	public void clickLoginButton() {
		this.getLoginElement().click();
	}

	public WebElement getAmaysimPasswordElement() {
		return this.driver.findElement(By.id("password"));
	}

	public void setAmaysimPassword(String msnpassword) {
		this.getAmaysimPasswordElement().sendKeys(msnpassword);
	}

}
