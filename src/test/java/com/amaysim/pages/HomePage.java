package com.amaysim.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public void clickMyAmaysimNavElement() {
		this.driver.findElement(By.id("homepage-nav-my_account")).click();
	}
}
