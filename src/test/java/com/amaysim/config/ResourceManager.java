package com.amaysim.config;

import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceManager {

	private static Map<String, Map<String, String>> url;
	private static Map<String, Map<String, Map<String, String>>> testData;
	private static final Logger LOG = LoggerFactory
			.getLogger(ResourceManager.class);

	/*
	 * static initializer load config data
	 */
	static {
		try {
			url = YamlHelper.loadUrlYamlFiles();
			testData = YamlHelper.loadTestDataYamlFiles();
			LOG.info("**** Running cucumber tests in " + Configs.ENV);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * return URL
	 */
	public Map<String, String> getUrl() {

		return url.get(Configs.ENV);
	}

	/*
	 * return test data
	 */
	public Map<String, Map<String, String>> getTestData() {
		return testData.get(Configs.ENV);
	}

}
