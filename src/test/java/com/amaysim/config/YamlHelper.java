package com.amaysim.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;

public class YamlHelper {

	public static Map<String, Map<String, String>> loadUrlYamlFiles()
			throws IOException {
		Map<String, Map<String, String>> url;
		String filePath = "src/test/resources/config/url.yml";
		url = loadYamlFiles(filePath);
		return url;

	}

	public static Map<String, Map<String, Map<String, String>>> loadTestDataYamlFiles()
			throws IOException {
		Map<String, Map<String, Map<String, String>>> testData;
		String filePath = "src/test/resources/config/testdata.yml";
		testData = loadYamlFiles(filePath);
		return testData;
	}

	/*
	 * 
	 * Parse the YAML file and return the output as a series of Maps and Lists
	 */
	private static Map loadYamlFiles(String filePath) throws IOException {
		File ymlFile = new File(filePath);
		Yaml yaml = new Yaml();

		try {
			InputStream ios = new FileInputStream(ymlFile);
			Map yamlData = (Map) yaml.load(ios);
			return yamlData;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

}
