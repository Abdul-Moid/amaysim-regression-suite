package com.amaysim.support;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-html-report",
		"json:target/cucumber-json-report.json", "rerun:target/rerun.txt" }, features = { "src/test/resources/features" }, glue = {
		"classpath:com.amaysim.stepdefinitions",
		"classpath:com.amaysim.support" }, tags = { "@login" })
public class RunCukesTest {

}