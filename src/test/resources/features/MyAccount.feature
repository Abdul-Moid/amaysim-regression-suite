@myaccount
Feature: As a amaysim customer
		 I want to login into my account
		 So that I see and change my settings
   
   Background:
   		Given I open amaysim home page
		When I navigate to login page
   
   @login
   Scenario: Verify that user can login into myaccount
		And I enter my valid login details
		Then I should see my account page
		And I logout
   
   @callforwarding
   Scenario: Verify that user can update call forwading settings
		And I enter my valid login details
		Then I should see my account page
		And I navigate to my settings page
		Then I update call forwading settings
		And I logout
		
	